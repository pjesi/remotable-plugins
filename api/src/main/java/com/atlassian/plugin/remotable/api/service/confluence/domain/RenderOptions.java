package com.atlassian.plugin.remotable.api.service.confluence.domain;

/**
 */
public interface RenderOptions
{
    void setStyle(RenderStyle style);
}
