<atlassian-plugin key="com.atlassian.labs.remoteapps-plugin" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <servlet-filter key="requestIdSetter" class="com.atlassian.plugin.remotable.plugin.util.http.bigpipe.RequestIdSettingFilter"
                    location="after-encoding">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="iframe-resources" class="com.atlassian.plugin.remotable.plugin.iframe.StaticResourcesFilter"
            location="after-encoding">
        <url-pattern>/remotable-plugins/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="universal-binary-dispatching" class="bean:ubDispatchFilter"
                    location="after-encoding" weight="1000">
        <url-pattern>/app/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="api-scoping" class="com.atlassian.plugin.remotable.plugin.module.permission.ApiScopingFilter"
            location="before-decoration">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="oauth-2lo-authentication" class="com.atlassian.plugin.remotable.plugin.module.oauth.OAuth2LOFilter"
            location="after-encoding" weight="2000">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet key="remotablePlugins2LORedirectingServlet"
             name="Remotable Plugins 2-Legged OAuth Signing and Redirecting Servlet"
             class="com.atlassian.plugin.remotable.plugin.module.util.redirect.RedirectServlet">
        <description>Redirects to Remotable Plugins, using 2-legged OAuth to sign the outgoing request.  Request parameters are included in the redirect.</description>
        <url-pattern>/redirect/oauth</url-pattern>
    </servlet>
    <servlet key="remotablePluginsPermanentRedirectingServlet"
             name="Remotable Plugins Permanent Redirecting Servlet"
             class="com.atlassian.plugin.remotable.plugin.module.util.redirect.RedirectServlet">
        <description>
            Redirects to a Pemote App resource, usually an image, with a permanent redirect, but
            doesn't sign the request.  Request parameters are included in the redirect.
        </description>
        <url-pattern>/redirect/permanent</url-pattern>
    </servlet>

    <web-resource key="iframe-host-js">
        <resource type="download" name="easyXDM.js" location="/js/iframe/easyXDM.js" />
        <resource type="download" name="ra-host.js" location="/js/iframe/host/ra-host.js" />
    </web-resource>

    <web-resource key="iframe-requirements">
        <context>remotable-plugins-iframe</context>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

    <web-resource key="dialog">
        <resource type="download" name="dialog.js" location="js/dialog/dialog.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

    <web-resource key="iframe-host-css">
        <resource type="download" name="loading.gif" location="/css/iframe/host/loading.gif" />
        <resource type="download" name="ra-host.css" location="/css/iframe/host/ra-host.css" />
        <context>atl.general</context>
        <context>atl.admin</context>
    </web-resource>

    <web-resource key="remote-condition">
        <resource type="download" name="remote-condition.js" location="js/condition/remote-condition.js" />
        <dependency>com.atlassian.labs.remoteapps-plugin:big-pipe</dependency>
    </web-resource>

    <web-resource key="images">
        <resource type="download" name="images/" location="images/" />
    </web-resource>

    <web-resource key="dialog-page-resource">
        <resource type="download" name="dialog-binder.js" location="/js/dialog/binder.js" />
        <context>atl.general</context>
        <context>atl.admin</context>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.labs.remoteapps-plugin:dialog</dependency>
    </web-resource>

    <web-resource key="smoke-test">
        <resource type="download" content-type="application/xml" name="atlassian-plugin.xml" location="/remotable-plugins-smoke-test/atlassian-plugin.xml" />
        <resource type="download" name="general-page.html" content-type="text/html" location="/remotable-plugins-smoke-test/general-page.html" />
        <transformation extension="xml">
            <transformer key="baseUrlTransformer"/>
        </transformation>
    </web-resource>

    <web-resource key="schema-xsl">
        <resource type="download" content-type="application/xml" name="xs3p.xsl" location="/xsd/xs3p.xsl"/>
    </web-resource>

    <web-resource-transformer key="baseUrlTransformer" class="com.atlassian.plugin.remotable.plugin.integration.smoketest.BaseUrlTransformer"/>

    <!-- big pipe stuff -->
    <web-panel key="requestIdMetaTag" class="com.atlassian.plugin.remotable.plugin.util.http.bigpipe.RequestIdWebPanel" location="atl.header"/>

    <web-resource key="big-pipe">
        <resource type="download" name="big-pipe.js" location="js/bigpipe/big-pipe.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

    <component key="pluginUriResolver"
               interface="com.atlassian.webhooks.spi.plugin.PluginUriResolver" public="true"
               class="com.atlassian.plugin.remotable.plugin.module.webhook.RemotablePluginsPluginUriResolver" />

    <component key="waitableServiceTrackerFactory" class="com.atlassian.osgi.tracker.WaitableServiceTrackerFactory" />

    <component key="httpContentRetriever" class="com.atlassian.plugin.remotable.plugin.util.http.CachingHttpContentRetriever"
               public="true" interface="com.atlassian.plugin.remotable.plugin.util.http.HttpContentRetriever" />

    <component key="startableForPlugins" class="com.atlassian.plugin.remotable.plugin.loader.StartableForPlugins" public="true"
               interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <component key="productEventPublisher" class="com.atlassian.plugin.remotable.spi.event.product.ProductEventPublisher"
               public="true" interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <component key="httpResourceMounter" class="com.atlassian.plugin.remotable.plugin.loader.LocalHttpResourceMounterServiceFactory" interface="com.atlassian.plugin.remotable.api.service.HttpResourceMounter" public="true" />

    <component key="remotappsService" class="com.atlassian.plugin.remotable.plugin.DefaultRemotablePluginInstallationService"
               interface="com.atlassian.plugin.remotable.spi.RemotablePluginInstallationService" public="true" />

    <component key="remotePluginRequestSigner" class="com.atlassian.plugin.remotable.plugin.webhooks.RemotePluginRequestSigner"
               interface="com.atlassian.webhooks.spi.plugin.RequestSigner" public="true" />

    <component key="serverWebHookProvider"
               interface="com.atlassian.webhooks.spi.provider.WebHookProvider" public="true"
               class="com.atlassian.plugin.remotable.plugin.webhooks.ServerWebHookProvider" />

    <component key="pluginsWebHookProvider"
                   interface="com.atlassian.webhooks.spi.provider.WebHookProvider" public="true"
                   class="com.atlassian.plugin.remotable.plugin.webhooks.PluginsWebHookProvider" />

    <component key="bundleLocator" class="com.atlassian.plugin.remotable.host.common.util.BundleContextBundleLoader" />
    <component key="permissionsReader" class="com.atlassian.plugin.remotable.host.common.descriptor.DescriptorPermissionsReader" />

    <component-import key="templateRenderer" interface="com.atlassian.templaterenderer.TemplateRenderer"/>
    <component-import key="servletModuleManager" interface="com.atlassian.plugin.servlet.ServletModuleManager" />
    <component-import key="webInterfaceManager" interface="com.atlassian.plugin.web.WebInterfaceManager" />
    <component-import key="pluginRetrievalService" interface="com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService" />
    <component-import key="oauthConsumerService" interface="com.atlassian.oauth.consumer.ConsumerService" />
    <component-import key="requestFactory" interface="com.atlassian.sal.api.net.RequestFactory" />
    <component-import key="pluginController" interface="com.atlassian.plugin.PluginController" />
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="serviceProviderConsumerStore" interface="com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore" />
    <component-import key="applicationLinkService" interface="com.atlassian.applinks.spi.link.MutatingApplicationLinkService" />
    <component-import key="authenticationConfigurationManager" interface="com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager" />
    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="authenticationListener" interface="com.atlassian.sal.api.auth.AuthenticationListener" />
    <component-import key="authenticationController" interface="com.atlassian.sal.api.auth.AuthenticationController" />
    <component-import key="transactionTemplate" interface="com.atlassian.sal.api.transaction.TransactionTemplate" />
    <component-import key="pluginEventManager" interface="com.atlassian.plugin.event.PluginEventManager" />
    <component-import key="pluginAccessor" interface="com.atlassian.plugin.PluginAccessor" />
    <component-import key="webResourceManager" interface="com.atlassian.plugin.webresource.WebResourceManager" />
    <component-import key="eventPublisher" interface="com.atlassian.event.api.EventPublisher" />
    <component-import key="i18nResolver" interface="com.atlassian.sal.api.message.I18nResolver" />
    <component-import key="localeResolver" interface="com.atlassian.sal.api.message.LocaleResolver" />
    <component-import key="pluginSettingsFactory" interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" />
    <component-import key="typeAccessor" interface="com.atlassian.applinks.spi.util.TypeAccessor" />
    <component-import key="webResourceUrlProvider" interface="com.atlassian.plugin.webresource.WebResourceUrlProvider" />
    <component-import key="moduleFactory" interface="com.atlassian.plugin.module.ModuleFactory" />
    <component-import key="httpClientFactory" interface="com.atlassian.httpclient.api.factory.HttpClientFactory" />
    <component-import key="httpClient" interface="com.atlassian.httpclient.api.HttpClient" />

    <!-- Cross-product API Scopes -->
    <plugin-permission key="read_app_links" name="Read Application Links"
        class="com.atlassian.plugin.remotable.plugin.product.ReadAppLinksScope">
        <description>Allows a Remotable Plugin to retrieve the host application's configured application links and entity links.</description>
    </plugin-permission>

    <plugin-permission key="modify_app_link" name="Modify Owned App Link"
        class="com.atlassian.plugin.remotable.plugin.product.ModifyAppLinkScope">
        <description>Allows a Remotable Plugin to modify the details of its own configured Application Link.</description>
    </plugin-permission>

    <!-- speakeasy integration -->
    <commonjs key="speakeasy-commonjs" location="/js/speakeasy" />

    <web-resource key="speakeasy-integration">
        <context>speakeasy.user-profile</context>
        <dependency>com.atlassian.labs.remoteapps-plugin:remotableplugins_installer</dependency>
    </web-resource>

    <web-item key="speakeasy-install" section="speakeasy.user-profile/actions" weight="100">
        <label>Install Remotable Plugin</label>
        <link linkId="rp-install" />
        <condition class="com.atlassian.plugin.remotable.plugin.integration.speakeasy.CanInstallRemotePluginsCondition" />
    </web-item>

    <!-- universal binary services -->
    <component key="localSignedRequestHandler" interface="com.atlassian.plugin.remotable.api.service.SignedRequestHandler"
               class="com.atlassian.plugin.remotable.plugin.service.LocalSignedRequestHandlerServiceFactory" public="true" />

    <component key="requestContext" interface="com.atlassian.plugin.remotable.api.service.RequestContext"
               class="com.atlassian.plugin.remotable.host.common.service.RequestContextServiceFactory" public="true" />

    <component key="renderContext" interface="com.atlassian.plugin.remotable.api.service.RenderContext"
               class="com.atlassian.plugin.remotable.host.common.service.RenderContextServiceFactory" public="true" />

    <component key="hostHttpClient" interface="com.atlassian.plugin.remotable.api.service.http.HostHttpClient"
               class="com.atlassian.plugin.remotable.host.common.service.http.HostHttpClientServiceFactory" public="true" />

    <component key="hostXmlRpcClient" interface="com.atlassian.plugin.remotable.api.service.http.HostXmlRpcClient"
               class="com.atlassian.plugin.remotable.host.common.service.http.HostXmlRpcClientServiceFactory" public="true" />

    <component key="localEmailSender" interface="com.atlassian.plugin.remotable.api.service.EmailSender"
               class="com.atlassian.plugin.remotable.plugin.service.LocalEmailSenderServiceFactory" public="true" />

    <applinks-application-type name="Remote Plugin Container" key="remote-plugin-container-type"
                                   class="com.atlassian.plugin.remotable.plugin.module.applinks.RemotePluginContainerApplicationTypeImpl"
                                   interface="com.atlassian.plugin.remotable.spi.applinks.RemotePluginContainerApplicationType">
            <manifest-producer class="com.atlassian.plugin.remotable.plugin.module.applinks.RemotePluginContainerManifestProducer"/>
    </applinks-application-type>

    <!-- remote extension points -->

    <described-module-type key="remote-plugin-container" name="Remote Plugin Container"
                               class="com.atlassian.plugin.remotable.plugin.module.applinks.RemotePluginContainerModuleDescriptor">
        <description>The server that is hosting this plugin</description>
        <optional-permissions>
            <permission>create_oauth_link</permission>
        </optional-permissions>
    </described-module-type>

    <described-module-type key="general-page" class="com.atlassian.plugin.remotable.plugin.module.page.GeneralPageModuleDescriptor">
        <description>A non-admin general page decorated by the application, with a link in a globally-accessible place</description>
    </described-module-type>

    <described-module-type key="admin-page" name="Administration Page" class="com.atlassian.plugin.remotable.plugin.module.page.AdminPageModuleDescriptor">
        <description>An admin page decorated in the admin section, with a link in the admin menu</description>
    </described-module-type>

    <described-module-type key="configure-page" class="com.atlassian.plugin.remotable.plugin.module.page.ConfigurePageModuleDescriptor" max-occurs="1">
        <description>The configuration page for the app, decorated in the admin section, with a link in the admin menu and a configure link in the Plugin Manager</description>
    </described-module-type>

    <described-module-type key="dialog-page" class="com.atlassian.plugin.remotable.plugin.module.page.dialog.DialogPageModuleDescriptor">
        <description>Loads a remote URL (iframe-wrapped) in an AUI Dialog</description>
    </described-module-type>

    <described-module-type key="plugin-permission" name="Plugin Permission"
                                   class="com.atlassian.plugin.remotable.spi.permission.PermissionModuleDescriptor">
        <description>A permission that needs to be requested by a plugin in order to execute certain functionality</description>
        <required-permissions>
            <permission>execute_java</permission>
        </required-permissions>
    </described-module-type>

    <plugin-permission key="create_oauth_link" name="Create OAuth Link">
        <description>Ability to create an incoming OAuth link that will allow trusted access</description>
    </plugin-permission>

    <plugin-permission key="execute_java" name="Execute Java">
        <description>Ability to execute arbitrary Java code on the Atlassian server</description>
        <!-- todo: add scanning -->
    </plugin-permission>

    <plugin-permission key="send_email" name="Send Email">
        <description>Ability to send an email from the Atlassian server</description>
    </plugin-permission>

    <!-- product-specific modules included via xslt build process -->

</atlassian-plugin>
