<html>
<head>
  <link rel="stylesheet" type="text/css" href="{{baseurl}}/remotable-plugins/all.css">
  <script src="{{baseurl}}/remotable-plugins/all.js" type="text/javascript"></script>
</head>
<body>
<div id="message">{{message}}</div>
<script type="text/javascript">
  RA.init();
</script>
</body>
</html>