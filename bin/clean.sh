#!/bin/sh

rm -rf `find . -name "data"`
rm -rf `find . -name ".cache"`
rm -rf `find . -name "attachments"`
