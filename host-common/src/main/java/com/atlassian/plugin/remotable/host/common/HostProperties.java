package com.atlassian.plugin.remotable.host.common;

/**
 *
 */
public interface HostProperties
{
    String getKey();
}
